// prettier.config.js or .prettierrc.js
module.exports = {
	useTabs: true,
	semi: true,
	singleQuote: true,
	trailingComma: 'es5',
	printWidth: 110,
};
