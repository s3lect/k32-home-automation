import { createApp, watch } from 'vue';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import 'primevue/resources/themes/saga-orange/theme.css';
import ToastService from 'primevue/toastservice';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;
import App from '@/App.vue';
import { useStore } from '@/store';

const app = createApp({
	components: { App },
	template: '<App />',
	setup() {
		const { initMqtt, state } = useStore();
		watch(state, async storeState => {
			console.log('state update', name, JSON.parse(JSON.stringify(storeState)));
		});
		initMqtt();
	},
	mounted() {
		// Hide the splash (you should do this on app launch)
	}
});
app.use(ToastService);
app.mount('#app');
SplashScreen.hide();
