import { reactive, readonly } from 'vue';
const mqtt = require('mqtt');

let mqttClient;

let state = reactive({
	'rollo-wohnzimmer': {
		POWER1: 'OFF',
		POWER2: 'OFF',
		Temperature: 0,
		Humidity: 0,
		Uptime: '',
	},
	'rollo-gastzimmer': {
		POWER1: 'OFF',
		POWER2: 'OFF',
		Uptime: '',
	},
	steckdose: {
		POWER: 'OFF',
		Uptime: '',
	},
	steckdose2: {
		POWER: 'OFF',
		Uptime: '',
	},
	'keller-checker': {
		Temperature: 0,
		Humidity: 0,
		Uptime: '',
	},
	'wohnzimmer-lampe': {
		POWER: 'OFF',
		type: 'shelly',
	},
	dht11: {'keller-checker': '', 'rollo-wohnzimmer': ''},
	dht11History: { 'keller-checker': {}, 'rollo-wohnzimmer': {} },
});

export function useStore() {
	function updateState(data) {
		Object.entries(data).forEach(([key, value]) => {
			Object.assign(state[key], value);
		});
	}
	function updateStatePath({ device, attribute, data }) {
		state[device][attribute] = data;
	}
	function updateDht11({ device, date, dht11 }) {
		state.dht11History[device][date] = dht11;
		state.dht11[device] = dht11;
	}
	async function initMqtt() {
		mqttClient = mqtt.connect('ws://wdr4300:8083');
		mqttClient.subscribe('home/stats/update');
		mqttClient.subscribe('home/dht11/update');
		mqttClient.subscribe('stat/#');
		mqttClient.subscribe('shellies/#');
		mqttClient.on('message', (topic, _payload) => {
			console.log('topic', topic);
			const text = new TextDecoder('utf-8').decode(_payload);
			let payload;
			try {
				payload = JSON.parse(text);
			} catch (error) {
				console.warn(`MQTT JSON payload: ${text} ${error}`);
				payload = text;
			}
			const device = topic.split('/')[1];
			console.log('device, payload', device, payload);
			if (topic.endsWith('relay/0')) {
				updateStatePath({ device, attribute: 'POWER', data: payload });
			} else if (topic.startsWith('stat')) {
				const [attribute, data] = Object.entries(payload)[0];
				updateStatePath({ device, attribute, data });
			} else if (topic === 'home/stats/update') {
				console.log("'home/stats/update'", payload);
				updateState(payload);
			} else if (topic === 'home/dht11/update') {
				updateDht11(payload);
			}
			// client.end();
		});
		// get the current state
		setTimeout(() => {
			mqttClient.publish('home/stats/get', 'things');
		},1000)
	}
	function toggleSwitch({ device }) {
		if (state[device].type === 'shelly') {
			mqttClient.publish(`shellies/${device}/relay/0/command`, state[device].POWER === 'on' ? 'off' : 'on');
		} else {
			mqttClient.publish(`cmnd/${device}/power`, state[device].POWER === 'ON' ? 'OFF' : 'ON');
		}
	}
	function toggleDual({ device, attribute }) {
		mqttClient.publish(`cmnd/${device}/${attribute}`, state[device][attribute] === 'ON' ? 'OFF' : 'ON');
	}
	function getHistoricalDht11({ device, date }) {
		console.log('getHistoricalDht11', date);
		if (date in state.dht11History[device]) {
			updateDht11({ device, date, dht11: state.dht11History[date] });
		} else {
			mqttClient.publish('home/dht11/history', `${device}/${date}`);
		}
	}
	return {
		initMqtt,
		getHistoricalDht11,
		toggleDual,
		toggleSwitch,
		state: readonly(state),
	};
}
