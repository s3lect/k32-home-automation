const CircularDependencyPlugin = require('circular-dependency-plugin');

module.exports = {
	runtimeCompiler: true,
	configureWebpack: (config) => {
		config.plugins.push(
			new CircularDependencyPlugin({
				exclude: /node_modules/,
				failOnError: true,
				// allow import cycles that include an asyncronous import,
				// e.g. via import(/* webpackMode: "weak" */ './file.js')
				allowAsyncCycles: false,
			})
		);
	},
	devServer: {
		overlay: false,
		// disableHostCheck: true, // allow local addresses `http://[computer name]:8080/`
		// https: true,  // use this to get an https dev server
	},
};
